package routes

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// import(
// 	"github.com/gorilla/mux"
// )

type PokemonRoutes struct {
	MyRouter http.Handler
}

func (pokemon *PokemonRoutes) Routes() {
	pokemon.MyRouter = mux.NewRouter()

	// poki := pokemon.MyRouter.PathPrefix("/pokemon2").Subrouter()
	// poki.HandleFunc("/2", Insertar1).Methods("GET")
	// router := mux.NewRouter()
	// // router := pokemon.myRouter
	// pokeRoutes := router.PathPrefix("/pokemon2").Subrouter()

	// pokeRoutes.HandleFunc("/2", Insertar1).Methods("GET")
	// pokeRoutes.HandleFunc("/3", Insertar2).Methods("GET")
}

func Insertar1(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "des poke router 1")
}

func Insertar2(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "des poke router 2")
}
