package server

import (
	"context"
	"net/http"
	"time"

	"bitbucket.org/dash128/PokeDash-API/src/app/controller"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/gorilla/mux"
	"github.com/mongodb/mongo-go-driver/mongo"
)

type Server interface {
	Router() http.Handler
}

type Api struct {
	router http.Handler
}

func (api *Api) Router() http.Handler {
	return api.router
}

var client *mongo.Client

func InitServer() Server {
	api := &Api{}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ = mongo.Connect(ctx, clientOptions)

	router := mux.NewRouter()

	pokemonController := controller.PokemonController{}
	pokemonRouter := router.PathPrefix("/pokemon").Subrouter()
	pokemonRouter.HandleFunc("/", pokemonController.Listar).Methods("GET")
	pokemonRouter.HandleFunc("/{id}", pokemonController.ListarPorId).Methods("GET")
	pokemonRouter.HandleFunc("/", pokemonController.Registrar).Methods("POST")
	pokemonRouter.HandleFunc("/", pokemonController.Actualizar).Methods("PUT")
	pokemonRouter.HandleFunc("/{id}", pokemonController.Eliminar).Methods("DELETE")

	api.router = router
	return api
}
