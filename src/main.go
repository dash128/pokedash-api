package main

import (
	"log"
	"net/http"

	"bitbucket.org/dash128/PokeDash-API/src/config/server"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	api := server.InitServer()
	// client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	log.Fatal(http.ListenAndServe(":9000", api.Router()))
}
