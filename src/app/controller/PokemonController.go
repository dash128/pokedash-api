package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/dash128/PokeDash-API/src/app/repository"
)

type PokemonController struct {
	repository repository.PokemonRepository
}

func (controller *PokemonController) Registrar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Registrar pokemon ")
}

func (controller *PokemonController) Actualizar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Actualizar pokemon ")
}

func (controller *PokemonController) Eliminar(rresponsees http.ResponseWriter, request *http.Request) {
	fmt.Print("Eliminar pokemon ")
}

func (controller *PokemonController) Listar(response http.ResponseWriter, request *http.Request) {
	pokemon := controller.repository.Listar()
	fmt.Print("Listar pokemon " + pokemon.Nombre)
	json.NewEncoder(response).Encode(pokemon)
}

func (controller *PokemonController) ListarPorId(response http.ResponseWriter, request *http.Request) {
	fmt.Print("ListarPorId pokemon ")
}
