package controller

import (
	"fmt"
	"net/http"
)

type EntrenadorController struct {
}

func (controller *EntrenadorController) Registrar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Registrar entrenador ")
}

func (controller *EntrenadorController) Actualizar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Actualizar entrenador ")
}

func (controller *EntrenadorController) Eliminar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Eliminar entrenador ")
}

func (controller *EntrenadorController) Listar(response http.ResponseWriter, request *http.Request) {
	fmt.Print("Listar entrenador ")
}

func (controller *EntrenadorController) ListarPorId(response http.ResponseWriter, request *http.Request) {
	fmt.Print("ListarPorId entrenador ")
}
