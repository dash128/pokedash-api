package model

import "github.com/mongodb/mongo-go-driver/bson/primitive"

type Pokemon struct {
	Id     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Nombre string             `json:"nombre,omitempty" bson:"nombre,omitempty"`
}
